DELIMITER &
CREATE PROCEDURE mergeProtectCoversTables(
IN m1DB VARCHAR(20) CHARSET utf8, 
IN m1FreshDB VARCHAR(20) CHARSET utf8
)
BEGIN
    DECLARE finished INTEGER DEFAULT 0;
DECLARE tableName varchar(100) DEFAULT "";
DECLARE tableKey varchar(100) DEFAULT "";
-- declare cursor
DEClARE curTable 
	CURSOR FOR 
    	SELECT 'cataloginventory_stock_item' AS 'value'
        union all SELECT 'catalog_compare_item' AS 'value'
        union all SELECT 'customer_address_entity' AS 'value'
        union all SELECT 'customer_address_entity_datetime' AS 'value'
            union all SELECT 'customer_address_entity_decimal' AS 'value'
            union all SELECT 'customer_address_entity_int' AS 'value'
            union all SELECT 'customer_address_entity_text' AS 'value'
            union all SELECT 'customer_address_entity_varchar' AS 'value'
            union all SELECT 'customer_entity' AS 'value'
            union all SELECT 'customer_entity_datetime' AS 'value'
            union all SELECT 'customer_entity_decimal' AS 'value'
            union all SELECT 'customer_entity_int' AS 'value'
            union all SELECT 'customer_entity_text' AS 'value'
            union all SELECT 'customer_entity_varchar' AS 'value'
            union all SELECT 'downloadable_link_purchased' AS 'value'
            union all SELECT 'downloadable_link_purchased_item' AS 'value'
            union all SELECT 'eav_entity_store' AS 'value'
            union all SELECT 'gift_message' AS 'value'
            union all SELECT 'log_visitor' AS 'value'
            union all SELECT 'newsletter_subscriber' AS 'value'
            union all SELECT 'rating_option_vote' AS 'value'
            union all SELECT 'rating_option_vote_aggregated' AS 'value'
            union all SELECT 'report_compared_product_index' AS 'value'
            union all SELECT 'report_event' AS 'value'
            union all SELECT 'report_viewed_product_index' AS 'value'
            union all SELECT 'review' AS 'value'
            union all SELECT 'review_detail' AS 'value'
            union all SELECT 'review_entity_summary' AS 'value'
            union all SELECT 'review_store' AS 'value'
            union all SELECT 'sales_flat_creditmemo' AS 'value'
            union all SELECT 'sales_flat_creditmemo_grid' AS 'value'
            union all SELECT 'sales_flat_creditmemo_item' AS 'value'
            union all SELECT 'sales_flat_invoice' AS 'value'
            union all SELECT 'sales_flat_invoice_grid' AS 'value'
            union all SELECT 'sales_flat_invoice_item' AS 'value'
            union all SELECT 'sales_flat_order' AS 'value'
            union all SELECT 'sales_flat_order_address' AS 'value'
            union all SELECT 'sales_flat_order_grid' AS 'value'
            union all SELECT 'sales_flat_order_item' AS 'value'
            union all SELECT 'sales_flat_order_payment' AS 'value'
            union all SELECT 'sales_flat_order_status_history' AS 'value'
            union all SELECT 'sales_flat_quote' AS 'value'
            union all SELECT 'sales_flat_quote_address' AS 'value'
            union all SELECT 'sales_flat_quote_address_item' AS 'value'
            union all SELECT 'sales_flat_quote_item' AS 'value'
            union all SELECT 'sales_flat_quote_item_option' AS 'value'
            union all SELECT 'sales_flat_quote_payment' AS 'value'
            union all SELECT 'sales_flat_quote_shipping_rate' AS 'value'
            union all SELECT 'sales_flat_shipment' AS 'value'
            union all SELECT 'sales_flat_shipment_comment' AS 'value'
            union all SELECT 'sales_flat_shipment_grid' AS 'value'
            union all SELECT 'sales_flat_shipment_item' AS 'value'
            union all SELECT 'sales_flat_shipment_track' AS 'value'
            union all SELECT 'sales_order_tax' AS 'value'
            union all SELECT 'sales_order_tax_item' AS 'value'
            union all SELECT 'sales_payment_transaction' AS 'value'
            union all SELECT 'wishlist' AS 'value'
            union all SELECT 'wishlist_item' AS 'value'
            union all SELECT 'wishlist_item_option' AS 'value';
    -- declare NOT FOUND handler
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
    
    OPEN curTable;
    getTable: LOOP
        FETCH curTable INTO tableName;
        IF finished = 1 THEN 
            LEAVE getTable;
        END IF;

	SELECT k.COLUMN_NAME
	Into tableKey
	FROM information_schema.table_constraints t
	LEFT JOIN information_schema.key_column_usage k
	USING(constraint_name,table_schema,table_name)
	WHERE t.constraint_type='PRIMARY KEY'
    AND t.table_schema=m1DB
   	AND t.table_name=tableName
	limit 1;
        
    SET @SQL = CONCAT('insert into ',m1DB,'.',tableName,'
        select l.*
        from ',m1FreshDB,'.',tableName,' l
        left join ',m1DB,'.',tableName,' ol
        On l.',tableKey,' = ol.',tableKey,'
        where ol.',tableKey,' is null');

	PREPARE stmt FROM @SQL;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

    END LOOP getTable;
    CLOSE curTable;
END
&
DELIMITER ;