# Magento 1 to 2 migration

Stored Procedure for merge delta from fresh m1 DB to m1 with data migration tool triggers.
It can help you if you forgot that data migration tool creates triggers in m1 DB for tracking data which were added after data migration.